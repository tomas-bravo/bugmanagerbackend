const EmailService = require("../services/EmailService");

import { sequelize } from "../models/index";
import {
  software,
  usuario,
  imagen,
  descripcion,
  //ticket,
  rol,
  ticketusuario_aceptado,
  nivelprioridad,
  estado,
  equipo,
  reasignar,
  comentario,
} from "../models";
import { orm } from "../database";
import { Op } from "sequelize";
const type_informe = {
  avance: 1,
  final: 2,
};
const stateValue = {
  nuevo: 1,
  asignado: 2,
  progreso: 3,
  finalizado: 4,
  reasignar: 5,
};
module.exports = {
  async getAllTickets() {
    try {
      const tickets = await orm.AllTickets();
      return tickets.length == 0
        ? { statusCode: 200, error: "No se encontraron tickets" }
        : { statusCode: 200, data: tickets };
    } catch (error) {
      console.error(`ERROR: ${error}`);
      return {
        statusCode: 500,
        error: "Error interno del servidor",
      };
    }
  },

  async getTicketById(ticketId) {
    try {
      const ticket_byId = await orm.findTicketById(ticketId);
      return !ticket_byId
        ? { statusCode: 200, error: "Ticket no encontrado" }
        : { statusCode: 200, data: ticket_byId };
    } catch (error) {
      console.error(`ERROR: ${error}`);
      return {
        statusCode: 500,
        error: "Error interno del servidor",
      };
    }
  },
  async getTicketsAsignados() {
    try {
      const tickets = await orm.TicketsAsignados();
      return tickets.length == 0
        ? {
            statusCode: 200,
            error: "No se encontraron ticket asignados o en progreso",
          }
        : { statusCode: 200, data: tickets };
    } catch (error) {
      console.error(`ERROR: ${error}`);
      return {
        statusCode: 500,
        error: "Error interno del servidor",
      };
    }
  },
  async getTicketsAsignadosDev(devId) {
    try {
      const user = await orm.findUserById(devId);
      if (!user)
        return { statusCode: 200, error: "Desarrollador no encontrado" };
      const tickets = await orm.TicketsAsignados(user);
      return tickets.length == 0
        ? { statusCode: 200, error: "No hay tickets asignados o en progreso" }
        : { statusCode: 200, data: tickets };
    } catch (error) {
      throw error;
    }
  },
  //Get new tickets
  async getNewTickets() {
    try {
      const tickets = await orm.NewTickets();
      return tickets.length == 0
        ? { statusCode: 200, error: "No se encontraron tickets nuevos" }
        : { statusCode: 200, data: tickets };
    } catch (error) {
      throw error;
    }
  },
  async asignTicket(id, developer_id) {
    // Verificar si el ticket existe
    const ticket_byId = await orm.findTicketById(id); //await ticket.findByPk(id); //, { raw: false });
    if (!ticket_byId) return { statusCode: 200, error: "Ticket no encontrado" };
    // Verificar si el usuario existe
    const user = await orm.findUserById(developer_id); //usuario.findByPk(developer_id);
    if (!user) return { statusCode: 200, error: "Desarrollador no encontrado" };
    try {
      if (await orm.countTicketAceptado(ticket_byId, user))
        //await ticket_byId.countAceptado())
        return { statusCode: 200, error: "Ticket ya se encuentra asignado" };
    } catch (error) {
      console.error(`ERROR: ${error}`);
      return {
        statusCode: 500,
        error: "Error interno del servidor",
      };
    }
    //const t = await sequelize.transaction(); // se omite el caso de transaccion para unitest
    try {
      await orm.addTicketAceptado(ticket_byId, user); //await ticket_byId.addAceptado(user, { transaction: t });
      await orm.setEstadoTicket(ticket_byId); //await ticket_byId.setEstado(stateValue.aceptado, { transaction: t });
      // await ticket_byId.removeEquipo((await ticket_byId.getEquipos())[0], {
      //     transaction: t,
      // });
      //await t.commit();
      await EmailService.sendTicketStatusChangedEmail(
        ticket_byId.correo_reportero,
        ticket_byId.nombre_reportero,
        ticket_byId.titulo,
        "aceptado"
      );
      return { statusCode: 200, data: ticket_byId };
    } catch (error) {
      await t.rollback();
      console.error(`ERROR: ${error}`);
      return {
        statusCode: 500,
        error: "Error interno del servidor",
      };
    }
  },
  async obtnerDevTicketsPropuestos(devId) {
    try {
      const user = await orm.findUserById(devId); //usuario.findByPk(devId);
      if (!user)
        return { statusCode: 200, error: "Desarrollador no encontrado" };
      const teams = await orm.getEquiposByUser(user); //getEquipos({ joinTableAttributes: [] });//orm.getEquiposByUser(user); //
      if (teams.length == 0)
        return {
          statusCode: 200,
          error: "Desarrollador no pertenece a ningun equipo",
        };
      var results = [];
      for (const e of teams) {
        const prop = await orm.getTicketsPropByTeam(e);
        for (const t of prop) results.push(orm.toJSON()); /*t.toJSON()*/
      }
      //}
      return results.length == 0
        ? { statusCode: 200, error: "No hay tickets propuestos" }
        : { statusCode: 200, data: results };
    } catch (error) {
      throw error;
    }
  },

  async finishTicket(id) {
    return orm.finishTicket(id);
  },
  async storeNewTicket(
    titulo,
    nombre_reportero,
    correo_reportero,
    app_id,
    estado_id,
    detalles,
    pasos,
    imagenes
  ) {
    try {
      const result = await orm.createNewTicket(
        titulo,
        nombre_reportero,
        correo_reportero,
        estado_id,
        app_id,
        detalles,
        pasos,
        imagenes
      );
      return !result || !Object.keys(result).length
        ? { statusCode: 500, error: "No se pudo crear el ticket" }
        : { statusCode: 201, data: result };
    } catch (error) {
      console.error(`ERROR: ${error}`);
      throw error; // Lanza el error para ser capturado en el método de ruta
    }
  },
  async storeNewComment(contenido, usuario_id, ticket_id) {
    const result = await orm.createNewComment(contenido, ticket_id, usuario_id);

    return !result || !Object.keys(result).length
      ? { statusCode: 500, message: "Error Interno del Servidor" }
      : { statusCode: 201, data: result };
  },
  async reasignarTicket(ticketId, devId) {
    const result = await orm.reasignarTicket(ticketId, devId);
    return !result || !Object.keys(result).length
      ? { statusCode: 500, message: "Error Interno del Servidor" }
      : { statusCode: 200, data: result };
  },
  async proponerReasignarTicket(id, devId) {
    const result = await orm.proponerReasignarTicket(id, devId);
    return !result || !Object.keys(result).length
      ? { statusCode: 500, message: "Error Interno del Servidor" }
      : { statusCode: 200, data: result };
  },
};
