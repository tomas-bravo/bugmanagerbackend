import { informe, usuario, tipo } from '../models';

module.exports = {
    async  getInformeById(informeId) {
        try {

            const inf = await informe.findByPk(informeId,{
                attributes: ['id', 'titulo', 'contenido', 'createdAt'],
                include: [ 
                    {
                        model: usuario,
                        attributes: ['id', 'nombre', 'apellido', 'correo']
                    },
                    
                    {
                        model: tipo,
                        attributes: ['id', 'nombre']  
                    }
                    
                    
                ]
            });
    
            if (!inf) {
                return { statusCode: 404, error: "Informe no encontrado" };
            }
    
            return { statusCode: 200, data: inf };
    
        } catch (error) {
            console.error(error);
            return { statusCode: 500, error: "Error interno del servidor" };
        }
    }
}
