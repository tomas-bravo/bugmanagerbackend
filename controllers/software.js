import { software } from '../models'

const stateValue = {
    nuevo: 1,
    asignado: 2,
    progreso: 3,
    finalizado: 4,
    rechazado: 5,
};
module.exports = {
    async getAllApps() {
        try {
            const apps = await software.findAll({ attributes: ['id', 'nombre'] });

            return !apps ? { statusCode: 404, error: "No hay apps" } : { statusCode: 200, data: apps };
        }
        catch (error) {
            console.error(`ERROR: ${error}`);
            return {
                statusCode: 500,
                error: "Error interno del servidor"
            };
        }
    },

    async infoApp(id) {
        try {
            var App = await software.findByPk(id);
            if (!App) return { statusCode: 404, error: "Aplicación no encontrada" };
            let tickets = {
                app: App,
                totales: 0,
                nuevos: 0,
                asignados: 0,
                finalizados: 0
            }
            tickets.totales = await App.countTickets();
            tickets.asignados = await App.countTickets({ where: { estado_id: stateValue.asignado } });
            tickets.progreso = await App.countTickets({ where: { estado_id: stateValue.progreso } });
            tickets.finalizados = await App.countTickets({ where: { estado_id: stateValue.finalizado } });
            tickets.nuevos = await App.countTickets({ where: { estado_id: stateValue.nuevo } });

            return { statusCode: 200, data: tickets };
        } catch (error) {
            console.error(`ERROR: ${error}`);
            return {
                statusCode: 500,
                error: "Error interno del servidor"
            };
        }
    },
}