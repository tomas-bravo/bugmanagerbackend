import {
    nivelprioridad,
    ticket,
} from '../models'

import { orm } from "../database";

module.exports = {
    async getPrioridades() {
        try {
            const results = orm.findAll();
            return !results ? { statusCode: 404, error: "No hay tickets" } : { statusCode: 200, data: results };
        } catch (error) {
            console.error(`ERROR: ${error}`);
            return {
                statusCode: 500,
                error: "Error interno del servidor"
            };
        }
    },
    async updateTicketImportance(ticket_id, nivel) {
        try {
            const ticketToUpdate = await orm.findTicketById(ticket_id);
            if (!ticketToUpdate) {
                return { statusCode: 404, error: "Ticket not found" };
            }


            const updatedTicket = await orm.updateTicket(ticketToUpdate, nivel);
            return !updatedTicket
                ? { statusCode: 500, error: "Failed to update ticket importance" }
                : { statusCode: 200, data: { message: "Ticket importance updated successfully" } };
        } catch (error) {
            console.error(`ERROR: ${error}`);
            return {
                statusCode: 500,
                error: "Error interno del servidor",
            };
        }
    },
}
