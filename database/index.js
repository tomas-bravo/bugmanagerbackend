import {
  software,
  usuario,
  imagen,
  descripcion,
  ticket,
  rol,
  ticketusuario_aceptado,
  nivelprioridad,
  estado,
  equipo,
  reasignar,
  comentario,
} from "../models";
import { Op } from "sequelize";
const stateValue = {
  nuevo: 1,
  asignado: 2,
  progreso: 3,
  finalizado: 4,
  reasignar: 5,
};
export const orm = {
  async findTicketById(ticketId) {
    return await ticket.findByPk(ticketId, {
      attributes: { exclude: ["estado_id", "nivelPrioridad_id", "app_id"] },
      include: [
        {
          model: descripcion,
          attributes: ["detalles", "pasos"],
        },
        {
          model: imagen,
          attributes: ["url"],
        },
        {
          model: nivelprioridad,
        },
        {
          model: estado,
          attributes: ["nombre"],
        },
        {
          model: software,
          attributes: ["id", "nombre"],
        },
      ],
    });
  },

  async AllTickets() {
    return await ticket.findAll({ raw: true });
  },

  async TicketsAsignados() {
    return await ticket.findAll({
      where: {
        estado_id: {
          [Op.or]: [stateValue.asignado, stateValue.progreso],
        },
      },
      include: [
        {
          association: "asignado",
          attributes: ["id", "nombre", "apellido"],
        },
        { model: estado, attributes: ["nombre"] },
        { model: nivelprioridad, attributes: ["nombre"] },
        { model: software, attributes: ["id", "nombre"] },
      ],
      attributes: [
        "id",
        "titulo",
        "nombre_reportero",
        "correo_reportero",
        "updatedAt",
      ],
    });
  },

  async findUserById(developer_id) {
    return await usuario.findByPk(developer_id);
  },

  async getTicketsByUser(user) {
    return await user.getAsignado({
      include: [
        { model: estado, attributes: ["nombre"] },
        { model: nivelprioridad, attributes: ["nombre"] },
        { model: software, attributes: ["id", "nombre"] },
      ],
      attributes: [
        "id",
        "titulo",
        "nombre_reportero",
        "correo_reportero",
        "updatedAt",
      ],
    });
  },

  async NewTickets() {
    return await ticket.findAll({
      where: { estado_id: stateValue.nuevo },
      include: [
        { model: estado, attributes: ["nombre"] },
        { model: software, attributes: ["id", "nombre"] },
      ],
      attributes: [
        "id",
        "titulo",
        "nombre_reportero",
        "correo_reportero",
        "createdAt",
      ],
    });
  },

  async countTicketAceptado(ticket, user) {
    return await ticket.countTicketAceptado(user);
  },

  async addTicketAceptado(ticket, user) {
    return await ticket.addAceptado(user);
  },

  async setEstadoTicket(ticket) {
    return await ticket.setEstado(stateValue.aceptado);
  },

  async getEquiposByUser(user) {
    return await user.getEquipos({ joinTableAttributes: [] });
  },

  async getTicketsPropByTeam(team) {
    return await team.getTickets({
      include: [descripcion, imagen, estado, nivelprioridad],
      joinTableAttributes: [],
    });
  },

  toJSON: function (t) {
    t.toJSON();
  },

  async finishTicket(id) {
    try {
      const ticket_byId = await ticket.findByPk(id);

      const contAvance = await ticket_byId.countInformes({
        where: { tipo_id: type_informe.avance },
      });
      const contFinal = await ticket_byId.countInformes({
        where: { tipo_id: type_informe.final },
      });
      if (contAvance == 0 && !contFinal)
        return {
          statusCode: 400,
          error:
            "Ticket no puede ser finalizado, dado que no se han subido los informes necesarios",
        };
      const t = await sequelize.transaction();

      await ticket_byId.update({
        estado_id: stateValue.finalizado,
        transaction: t,
      });
      await ticket_byId.removeAceptado(null, { transaction: t });

      t.commit();

      await EmailService.sendTicketStatusChangedEmail(
        ticket_byId.correo_reportero,
        ticket_byId.nombre_reportero,
        ticket_byId.titulo,
        "finalizado"
      );
      return { statusCode: 200, data: ticket_byId };
    } catch (error) {
      t.rollback();
      return { statusCode: 500, message: "Error interno del Servidor" };
    }
  },

  async createNewTicket(
    titulo,
    nombre_reportero,
    correo_reportero,
    estado_id,
    app_id,
    detalles,
    pasos,
    imagenes
  ) {
    return sequelize.transaction(async (t) => {
      const newTicket = await ticket.create(
        {
          titulo: titulo,
          nombre_reportero: nombre_reportero,
          correo_reportero: correo_reportero,
          estado_id: estado_id,
          app_id: app_id,
          descripcion: {
            detalles: detalles,
            pasos: pasos,
          },
        },
        {
          include: {
            association: ticket.descripcion,
          },
          transaction: t,
        }
      );

      if (imagenes) {
        for (const image of imagenes) {
          await imagen.create(
            {
              url: image,
              ticket_id: newTicket.id,
            },
            { transaction: t }
          );
        }
      }
      return newTicket;
    });
  },

  async createNewComment(content, ticketId, userId) {
    try {
      const user = await usuario.findByPk(userId);
      const comment = await user.createComment(
        {
          ticketId: ticketId,
          content: content,
        },
        { raw: true }
      );
      return comment;
    } catch (err) {
      return null;
    }
  },
  async reasignarTicket(ticketId, devId) {
    const t = await sequelize.transaction();
    try {
      const bug = await ticket.findByPk(ticketId);
      const dev = await usuario.findByPk(devId);
      const state = await estado.findByPk(stateValue.reasignar);
      await bug.setEstado(state, { transaction: t });
      await bug.setAsignado(dev, { transaction: t });
      await t.commit();
      return bug;
    } catch (error) {
      await t.rollback();
      console.error(`ERROR: ${error}`);
      return null;
    }
  },
  async proponerReasignarTicket(ticketId, devId) {
    const t = await sequelize.transaction();
    try {
      const bug = await ticket.findByPk(id);
      const state = await estado.findByPk(stateValue.reasignar);
      const prop = await usuario.findByPk(devId);
      const dev = await bug.getAsignado();
      await reasignar.create(
        { usuarioId: dev.id, ticketId: bug.id, propuesta: prop.id },
        { transaction: t }
      );
      await bug.setEstado(state, { transaction: t });
      await bug.setAsignado(null, { transaction: t });
      await t.commit();
      return bug;
    } catch (error) {
      await t.rollback();
      console.error(`ERROR: ${error}`);
      return null;
    }
  },
  
  async findAll() {
    return await nivelprioridad.findAll({ attributes: ['id', 'nombre'] });
  },

  async updateTicket(ticket, nivel) {
    return await ticket.update({ nivelPrioridad_id: nivel });
  },
};
