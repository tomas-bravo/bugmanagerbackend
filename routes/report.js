const  reportController  = require('../controllers/report');

module.exports = (app) => {
    app.get('/report/:id', async (req, res) => {
        const { id } = req.params;
        
        try {
          const reportData = await reportController.getInformeById(id);
          
          if (reportData.error) {
            return res.status(reportData.statusCode).json({ message: reportData.error });
          }
      
          res.json(reportData);
        } catch (error) {
          console.error(error);
          res.status(500).json({ message: 'Error interno del servidor' });
        }
      });
    }