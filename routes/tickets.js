const ticketController = require("../controllers/tickets");

module.exports = (app) => {
  app.get("/api/tickets", async (req, res) => {
    //todo Si se pide un ticket en especifico. Tiene que ser mas especifico (imagen, descripcion, etc)
    const { ticketId } = req.query;
    var requestResult;
    try {
      if (ticketId)
        requestResult = await ticketController.getTicketById(ticketId);
      else requestResult = await ticketController.getAllTickets();
    } catch (error) {
      requestResult = { statusCode: 500, error: "Error interno del servidor" };
    }
    return !requestResult
      ? res.status(500).send({ error: "Error interno del servidor" })
      : requestResult.statusCode !== 200
      ? res.status(requestResult.statusCode).send(requestResult.error)
      : res.status(200).send(requestResult.data);
  });
  app.get("/api/tickets/asignados", async (req, res) => {
    const { devId } = req.query;
    // if (!userId) return res.status(400).send({ error: "userId missing" });
    var requestResult;
    try {
      if (devId)
        requestResult = await ticketController.getTicketsAsignadosDev(devId);
      // .lafuncion que creare
      else requestResult = await ticketController.getTicketsAsignados();
    } catch (error) {
      console.log(error);
      requestResult = { statusCode: 500, error: "Error interno del servidor" };
    }
    return !requestResult
      ? res.status(500).send({ error: "Error interno del servidor" })
      : requestResult.statusCode !== 200
      ? res.status(requestResult.statusCode).send(requestResult.error)
      : res.status(200).send(requestResult.data);
  });

  app.get("/api/tickets/reasignar", async (req, res) => {
    const { devId } = req.query;
    let requestResult;
    try {
      if (devId)
        requestResult = await ticketController.obtnerDevTicketsPropuestos(
          devId
        );
      // .lafuncion que creare
      else requestResult = await ticketController.getTicketsPropuestos(); // .lafuncion que creare
    } catch (error) {
      console.log(error);
      requestResult = { statusCode: 500, error: "Error interno del servidor" };
    }
    return !requestResult
      ? res.status(500).send({ error: "Error interno del servidor" })
      : requestResult.statusCode !== 200
      ? res.status(requestResult.statusCode).send(requestResult.error)
      : res.status(200).send(requestResult.data);
  });

  app.post("/api/tickets/reasignar", async (req, res) => {
    const { ticketId, devId } = req.body;
    if (!ticketId || !devId)
      return res.status(400).send({ error: "Falta ticketId o devId en body" });
    let requestResult;
    try {
      requestResult = await ticketController.proponerReasignarTicket(
        ticketId,
        devId
      );
    } catch (error) {
      console.log(error);
      requestResult = null;
    }
    return !requestResult
      ? res.status(500).send({ error: "Error interno del servidor" })
      : requestResult.statusCode !== 201
      ? res.status(requestResult.statusCode).send(requestResult.error)
      : res.status(requestResult.statusCode).send(requestResult.data);
  });
  //route to get all new tickets
  app.get("/api/tickets/news", async (req, res) => {
    var requestResult;
    try {
      requestResult = await ticketController.getNewTickets();
    } catch (error) {
      requestResult = { statusCode: 500, error: "Error interno del servidor" };
    }
    return !requestResult
      ? res.status(500).send({ error: "Error interno del servidor" })
      : requestResult.statusCode !== 200
      ? res.status(requestResult.statusCode).send(requestResult.error)
      : res.status(200).send(requestResult.data);
  });

  app.patch("/api/tickets/asigne", async (req, res) => {
    const { id, developerId } = req.body;
    console.log(id, developerId);
    let requestResult;
    try {
      requestResult = await ticketController.asignTicket(id, developerId);
    } catch (error) {
      requestResult = { statusCode: 500, error: "Error interno del servidor" };
    }
    return !requestResult
      ? res.status(500).send({ error: "Error interno del servidor" })
      : requestResult.statusCode !== 200
      ? res.status(requestResult.statusCode).send(requestResult.error)
      : res.status(200).send(requestResult.data);
  });
  app.patch("/api/tickets/finalizar", async (req, res) => {
    const { ticketId, usuarioId } = req.body;
    let requestResult;
    try {
      requestResult = await ticketController.finishTicket(ticketId, usuarioId);
    } catch (error) {
      requestResult = { statusCode: 500, error: "Error interno del servidor" };
    }
    return !requestResult
      ? res.status(500).send({ error: "Error interno del servidor" })
      : requestResult.statusCode !== 200
      ? res.status(requestResult.statusCode).send(requestResult.error)
      : res.status(200).send(requestResult.data);
  });

  app.post("/api/tickets/create", async (req, res) => {
    const {
      titulo,
      nombre_reportero,
      correo_reportero,
      app_id,
      estado_id,
      detalles,
      pasos,
    } = req.body;
    const { files } = req;
    const fileNames = [];
    try {
      if (files) {
        fileNames = files.files.map((e) => e.name);
        for (const file of files.files)
          file.mv(`${__dirname}/../images/${file.name}`, (err) => {
            if (err) {
              console.log(err);
              res.status(500).send({ error: "Error interno del servidor" });
            }
          });
      }
      const newTicket = await ticketController.storeNewTicket(
        titulo,
        nombre_reportero,
        correo_reportero,
        app_id,
        estado_id,
        detalles,
        pasos,
        fileNames
      );
      res.status(200).send(newTicket);
      // res.status(500).send({ error: "Error interno del servidor" });
    } catch (error) {
      res.status(500).send({ error: "Error interno del servidor" });
    }
  });
  app.patch("/api/tickets/proponer", async (req, res) => {
    const { ticketId, devId, prioridadId } = req.body;
    if (!ticketId)
      return res.status(400).send({ error: "Falta ticketId en el body" });
    if (!devId)
      return res.status(400).send({ error: "Falta devId en el body" });
    if (!prioridadId)
      return res.status(400).send({ error: "Falta prioridadId en el body" });
    let requestResult;
    try {
      requestResult = await ticketController.proponerTicketToDev(
        ticketId,
        devId,
        prioridadId
      );
    } catch (error) {
      requestResult = { statusCode: 500, error: "Error interno del servidor" };
    }
    return !requestResult
      ? res.status(500).send({ error: "Error interno del servidor" })
      : requestResult.statusCode !== 200
      ? res.status(requestResult.statusCode).send(requestResult.error)
      : res.status(200).send(requestResult.data);
  });
  app.patch("/api/tickets/rechazar", async (req, res) => {
    const { ticketId } = req.body;
    if (!ticketId)
      return res.status(404).send({ error: "No se a entregado ticketId" });
    var requestResult;
    try {
      requestResult = await ticketController.rechazarTicket(ticketId);
    } catch (error) {
      console.log(error);
      requestResult = { statusCode: 500, error: "Error interno del servidor" };
    }
    return !requestResult
      ? res.status(500).send({ error: "Error interno del servidor" })
      : requestResult.statusCode !== 200
      ? res.status(requestResult.statusCode).send(requestResult.error)
      : res.status(200).send(requestResult.data);
  });

  app.post("/api/tickets/:ticketId/comments", async (req, res) => {
    let requestResult;
    const { contenido, usuario_id } = req.body;
    const { ticketId } = req.params;

    try {
      requestResult = await ticketController.storeNewComment(
        contenido,
        usuario_id,
        ticketId
      );
    } catch (error) {
      requestResult = { statusCode: 500, error: "Error interno del servidor" };
    }

    return !requestResult
      ? res.status(500).send({ error: "Error interno del servidor" })
      : requestResult.statusCode !== 200
      ? res.status(requestResult.statusCode).send(requestResult.error)
      : res.status(200).send(requestResult.data);
  });
};
