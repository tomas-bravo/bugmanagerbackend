//ruta para llamar al controlador equipo.js
const equipoController = require('../controllers/equipos')

module.exports = (app)=>{
    app.get("/api/equipos/:proyectoId", async (req, res) => {
        const { proyectoId } = req.params;
        var requestResult;
        try {
            requestResult = await equipoController.getEquiposByProyectoId(proyectoId);
        }
        catch (error) {
            requestResult = { statusCode: 500, error: "Error interno del servidor" };
        }
        return !requestResult ? res.status(500).send({ error: "Error interno del servidor" }) : requestResult.statusCode !== 200 ? res.status(requestResult.statusCode).send(requestResult.error) : res.status(200).send(requestResult.data);
    });


}