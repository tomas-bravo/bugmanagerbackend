require('dotenv').config()
const nodemailer = require('nodemailer');
const mailTemplates = require('../templates/mailTemplates.js');


const EmailService = (function () {

    async function sendMail(mailAddress, mailTemplate) {
        if (process.env.MAIL_ACTIVE) {
            console.log("Mail not sent. MAIL_ACTIVE is false");
            return;
        }
        const transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: process.env.MAIL_USERNAME,
                pass: process.env.MAIL_PASSWORD
            }
        });

        const mailOptions = {
            from: process.env.MAIL_USERNAME,
            to: mailAddress,
            subject: mailTemplate.subject,
            html: mailTemplate.body
        };

        try {
            const info = await transporter.sendMail(mailOptions);
            console.log("Message sent: %s", info.messageId);
        } catch (error) {
            console.error("Error sending email:", error);
        }
    }


    async function sendWelcomeEmail(mailAddress, name, content) {
        const mailTemplate = mailTemplates.welcomeEmail(name, content);
        await sendMail(mailAddress, mailTemplate);
    }

    async function sendTicketStatusChangedEmail(mailAddress, name, ticketId, content) {
        const mailTemplate = mailTemplates.changedTicketStatus(name, ticketId, content);
        await sendMail(mailAddress, mailTemplate);
    }




    return {
        sendMail,
        sendWelcomeEmail,
        sendTicketStatusChangedEmail
    }
}) ();

module.exports = EmailService;