function welcomeEmail(name, content) {
    const subject = "Welcome to our Platform";

    const body = `
        <html>
            <body>
                <h1>Welcome, ${name}!</h1>
                <p>${content}</p>
                <p>Best regards,</p>
                <p>Your Platform Team</p>
            </body>
        </html>
    `;

    return {
        subject,
        body
    };
}


function changedTicketStatus(name, ticketId, content) {
    const subject = "Se ha cambiado el estado de su ticket!";

    const body = `
        <html>
            <body>
                <h3>Hola, ${name}!</h3>
                <p>El estado de su ticket ${ticketId} ha cambiado a ${content}.</p>
                <br>
                <p>Saludos,</p>
                <p>El equipo de la plataforma</p>
            </body>
        </html>
    `;

    return {
        subject,
        body
    };
}

module.exports = {
    welcomeEmail,
    changedTicketStatus
};