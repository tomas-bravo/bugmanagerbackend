import controller from "../../controllers/nivelImportancia";
import { orm } from "../../database";
import ticket from '../../models/ticket';
const EmailService = require("../../services/EmailService");

jest.mock("../../database");
jest.mock("../../services/EmailService");

beforeEach(() => {
  jest.clearAllMocks();
});

describe('Test Controller Nivel Importancia', () => {
    describe('Test getPrioridades()', () => {
        test('Test 1: Comprobar getPrioridades()', async () => {
            orm.findAll.mockReturnValueOnce([]);
            const result = await controller.getPrioridades();
            expect(orm.findAll).toHaveBeenCalledTimes(1);
            expect(result.statusCode).toEqual(200);
        });
    });
       describe('Test updateTicketImportance()', () => {
        test('Test 1: Comprobar updateTicketImportance con ticket y nivel de prioridad existente.', async () => {
            const mockTicket = {
                id: 1,
                titulo: "Bug pantalla carga",
                nombre_reportero: "Andres Dellen",
                correo_reportero: "adede@gmail.com",
            };

            const nivel = 'alto';

            orm.findTicketById.mockReturnValueOnce(mockTicket);
            orm.updateTicket.mockReturnValueOnce(mockTicket);
            const result = await controller.updateTicketImportance(mockTicket.id, nivel)
            expect(orm.findTicketById).toHaveBeenCalledTimes(1);
            expect(orm.updateTicket).toHaveBeenCalledTimes(1);
            expect(result.statusCode).toEqual(200);
            expect(result.data.message).toEqual("Ticket importance updated successfully");
        });
        test('Test 2: Comprobar updateTicketImportance con ticket y pero sin nivel de prioridad.', async () => {
            const mockTicket = {
                id: 1,
                titulo: "Bug pantalla carga",
                nombre_reportero: "Andres Dellen",
                correo_reportero: "adede@gmail.com",
            };

            const nivel = 'alto';

            orm.findTicketById.mockReturnValueOnce(mockTicket.id);
            orm.updateTicket.mockReturnValueOnce(null);
            const result = await controller.updateTicketImportance(mockTicket.id, nivel)
            expect(orm.findTicketById).toHaveBeenCalledTimes(1);
            expect(orm.updateTicket).toHaveBeenCalledTimes(1);
            expect(result.statusCode).toEqual(500);
            expect(result.error).toEqual("Failed to update ticket importance");
        });
        test('Test 3: Comprobar updateTicketImportance sin ticket', async () => {
            const mockTicket = {
                id: 1,
                titulo: "Bug pantalla carga",
                nombre_reportero: "Andres Dellen",
                correo_reportero: "adede@gmail.com",
            };

            const nivel = 'alto';

            orm.findTicketById.mockReturnValueOnce(null);
            orm.updateTicket.mockReturnValueOnce(null);
            const result = await controller.updateTicketImportance(mockTicket.id, nivel)
            expect(orm.findTicketById).toHaveBeenCalledTimes(1);
            expect(result.statusCode).toEqual(404);
            expect(result.error).toEqual("Ticket not found");
        });
    });
});
