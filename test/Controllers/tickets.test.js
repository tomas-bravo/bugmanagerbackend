import controller from "../../controllers/tickets";
import { orm } from "../../database";
import ticket from "../../models/ticket";
const EmailService = require("../../services/EmailService");

jest.mock("../../database");
jest.mock("../../services/EmailService");

beforeEach(() => {
  jest.clearAllMocks();
});

// mocking finish ticket

describe("Test Controladores Tickets", () => {
  describe("Tests getAllTickets", () => {
    test("Test 1. Comprobar getAllTickets con tickets existentes", async () => {
      orm.AllTickets.mockReturnValueOnce([{ id: 1, nombre: "test" }]);
      const result = await controller.getAllTickets();
      expect(orm.AllTickets).toHaveBeenCalledTimes(1);
      expect(result.statusCode).toEqual(200);
    });
    test("Test 2. Comprobar getAllTickets sin tickets existentes", async () => {
      orm.AllTickets.mockReturnValueOnce([]);
      const result = await controller.getAllTickets();
      expect(orm.AllTickets).toHaveBeenCalledTimes(1);
      expect(result.statusCode).toEqual(200);
      expect(result.error).toEqual("No se encontraron tickets");
    });
  });

  describe("Tests getTicketById", () => {
    test("Test 1. Comprobar getTicketById con ticket existente", async () => {
      orm.findTicketById.mockReturnValueOnce({
        id: 1,
        titulo: "Bug pantalla carga",
        nombre_reportero: "Andres Dellen",
        correo_reportero: "adede@gmail.com",
      });
      const result = await controller.getTicketById(1);
      expect(orm.findTicketById).toHaveBeenCalledTimes(1);
      expect(result.statusCode).toEqual(200);
    });
    test("Test 2. Comprobar getTicketById con ticket no existente", async () => {
      orm.findTicketById.mockReturnValueOnce(null);
      const result = await controller.getTicketById(1);
      expect(orm.findTicketById).toHaveBeenCalledTimes(1);
      expect(result.statusCode).toEqual(200);
      expect(result.error).toEqual("Ticket no encontrado");
    });
  });

  describe("Tests getTicketsAsignados", () => {
    test("Test 1. Comprobar getTicketsAsignados con ticket existente", async () => {
      orm.TicketsAsignados.mockReturnValueOnce({
        id: 4,
        titulo: "Bug en la busqueda de elementos",
        nombre_reportero: "Kike Neira",
        correo_reportero: "kikolo@gmail.com",
      });
      const result = await controller.getTicketsAsignados();
      expect(orm.TicketsAsignados).toHaveBeenCalledTimes(1);
      expect(result.statusCode).toEqual(200);
    });

    test("Test 2. Comprobar getTicketsAsignados sin ticket existentes", async () => {
      orm.TicketsAsignados.mockReturnValueOnce([]);
      const result = await controller.getTicketsAsignados();
      expect(orm.TicketsAsignados).toHaveBeenCalledTimes(1);
      expect(result.statusCode).toEqual(200);
      expect(result.error).toEqual(
        "No se encontraron ticket asignados o en progreso"
      );
    });
  });

  describe("Tests getTicketAsignadosDev", () => {
    test("Test 1. Comprobar getTicketAsignadosDev con usuario y con tickets", async () => {
      orm.findUserById.mockReturnValueOnce({
        nombre: "Esteban",
        apellido: "Gonzales",
        correo: "egonza@gmail.com",
      });
      orm.TicketsAsignados.mockReturnValueOnce({
        id: 4,
        titulo: "Bug en la busqueda de elementos",
        nombre_reportero: "Kike Neira",
        correo_reportero: "kikolo@gmail.com",
      });
      const result = await controller.getTicketsAsignadosDev(1);
      expect(orm.findUserById).toHaveBeenCalledTimes(1);
      expect(orm.TicketsAsignados).toHaveBeenCalledTimes(1);
      expect(result.statusCode).toEqual(200);
    });
    test("Test 2 . Comprobar getTicketAsignadosDev con usuario y sin ticket", async () => {
      orm.findUserById.mockReturnValueOnce({
        nombre: "Esteban",
        apellido: "Gonzales",
        correo: "egonza@gmail.com",
      });
      orm.TicketsAsignados.mockReturnValueOnce([]);
      const result = await controller.getTicketsAsignadosDev(1);
      expect(orm.findUserById).toHaveBeenCalledTimes(1);
      expect(orm.TicketsAsignados).toHaveBeenCalledTimes(1);
      expect(result.statusCode).toEqual(200);
      expect(result.error).toEqual("No hay tickets asignados o en progreso");
    });
    test("Test 3 . Comprobar getTicketAsignadosDev sin ticket ni usuarios", async () => {
      orm.findUserById.mockReturnValueOnce(null);
      orm.TicketsAsignados.mockReturnValueOnce(null);
      const result = await controller.getTicketsAsignadosDev(1);
      expect(orm.findUserById).toHaveBeenCalledTimes(1);
      expect(orm.TicketsAsignados).toHaveBeenCalledTimes(0);
      expect(result.statusCode).toEqual(200);
    });
  });

  describe("Tests getNewTickets", () => {
    test("Test 1. Comprobar getNewTickets con ticket existente", async () => {
      orm.NewTickets.mockReturnValueOnce({
        nombre: "Esteban",
        apellido: "Gonzales",
        correo: "egonza@gmail.com",
      });
      const result = await controller.getNewTickets();
      expect(orm.NewTickets).toHaveBeenCalledTimes(1);
      expect(result.statusCode).toEqual(200);
    });
    test("Test 2. Comprobar getNewTickets sin ticket existente", async () => {
      orm.NewTickets.mockReturnValueOnce([]);
      const result = await controller.getNewTickets();
      expect(orm.NewTickets).toHaveBeenCalledTimes(1);
      expect(result.statusCode).toEqual(200);
      expect(result.error).toEqual("No se encontraron tickets nuevos");
    });
  });

  describe("Tests assignTickets", () => {
    test("Test 1. Comprobar assignTickets con ticket y dev existente caso feliz", async () => {
      orm.findTicketById.mockReturnValueOnce({
        id: 1,
        titulo: "Bug pantalla carga",
        nombre_reportero: "Andres Dellen",
        correo_reportero: "adede@gmail.com",
      });
      orm.findUserById.mockReturnValueOnce({
        nombre: "Esteban",
        apellido: "Gonzales",
        correo: "egonza@gmail.com",
      });
      orm.countTicketAceptado.mockReturnValueOnce(0);
      const result = await controller.asignTicket(1, 1);
      expect(orm.countTicketAceptado).toHaveBeenCalledTimes(1);
      expect(orm.addTicketAceptado).toHaveBeenCalledTimes(1);
      expect(orm.setEstadoTicket).toHaveBeenCalledTimes(1);
      expect(EmailService.sendTicketStatusChangedEmail).toHaveBeenCalledTimes(
        1
      );
      expect(result.statusCode).toEqual(200);
    });
    test("Test 2. Comprobar assignTickets sin ticket y dev existente", async () => {
      orm.findTicketById.mockReturnValueOnce(null);
      const result = await controller.asignTicket(1, 1);
      expect(orm.findTicketById).toHaveBeenCalledTimes(1);
      expect(orm.findUserById).toHaveBeenCalledTimes(0);
      expect(orm.countTicketAceptado).toHaveBeenCalledTimes(0);
      expect(orm.addTicketAceptado).toHaveBeenCalledTimes(0);
      expect(orm.setEstadoTicket).toHaveBeenCalledTimes(0);
      expect(EmailService.sendTicketStatusChangedEmail).toHaveBeenCalledTimes(
        0
      );
      expect(result.statusCode).toEqual(200);
      expect(result.error).toEqual("Ticket no encontrado");
    });

    test("Test 3. Comprobar assignTickets con ticket y sin dev existente", async () => {
      orm.findUserById.mockReturnValueOnce(null);
      orm.findTicketById.mockReturnValueOnce({
        id: 1,
        titulo: "Bug pantalla carga",
        nombre_reportero: "Andres Dellen",
        correo_reportero: "adede@gmail.com",
      });

      const result = await controller.asignTicket(1, 1);
      expect(orm.countTicketAceptado).toHaveBeenCalledTimes(0);
      expect(orm.addTicketAceptado).toHaveBeenCalledTimes(0);
      expect(orm.setEstadoTicket).toHaveBeenCalledTimes(0);
      expect(EmailService.sendTicketStatusChangedEmail).toHaveBeenCalledTimes(
        0
      );
      expect(result.statusCode).toEqual(200);
      expect(result.error).toEqual("Desarrollador no encontrado");
    });
    test("Test 4. Comprobar assignTickets con ticket y con dev existente pero ticket ya asignado", async () => {
      orm.findTicketById.mockReturnValueOnce({
        id: 1,
        titulo: "Bug pantalla carga",
        nombre_reportero: "Andres Dellen",
        correo_reportero: "adede@gmail.com",
      });
      orm.findUserById.mockReturnValueOnce({
        nombre: "Esteban",
        apellido: "Gonzales",
        correo: "egonza@gmail.com",
      });
      orm.countTicketAceptado.mockReturnValueOnce(1);
      const result = await controller.asignTicket(1, 1);
      expect(orm.findTicketById).toHaveBeenCalledTimes(1);
      expect(orm.findUserById).toHaveBeenCalledTimes(1);
      expect(orm.countTicketAceptado).toHaveBeenCalledTimes(1);
      expect(orm.addTicketAceptado).toHaveBeenCalledTimes(0);
      expect(orm.setEstadoTicket).toHaveBeenCalledTimes(0);
      expect(EmailService.sendTicketStatusChangedEmail).toHaveBeenCalledTimes(
        0
      );
      expect(result.statusCode).toEqual(200);
      expect(result.error).toEqual("Ticket ya se encuentra asignado");
    });
  });

  describe("Tests obtnerDevTicketsPropuestos", () => {
    test("Test 1. Comprobar obtnerDevTicketsPropuestos con desarrolador y con ticket propuestos a los equipos", async () => {
      orm.findUserById.mockReturnValueOnce({
        nombre: "Esteban",
        apellido: "Gonzales",
        correo: "egonza@gmail.com",
      });
      orm.getEquiposByUser.mockReturnValueOnce([
        {
          id: 1,
          nombre: "Equipo 1",
        },
      ]);
      orm.getTicketsPropByTeam.mockReturnValueOnce([
        {
          id: 1,
        },
      ]);
      const result = await controller.obtnerDevTicketsPropuestos(1);
      expect(orm.findUserById).toHaveBeenCalledTimes(1);
      expect(orm.getEquiposByUser).toHaveBeenCalledTimes(1);
      expect(orm.getTicketsPropByTeam).toHaveBeenCalledTimes(1);
      expect(result.statusCode).toEqual(200);
    });
    test("Test 2. Comprobar obtnerDevTicketsPropuestos con desarrolador y pero no pertenece a ningun equipo(dev nuevo)", async () => {
      orm.findUserById.mockReturnValueOnce({
        nombre: "Esteban",
        apellido: "Gonzales",
        correo: "egonza@gmail.com",
      });
      orm.getEquiposByUser.mockReturnValueOnce([]);
      const result = await controller.obtnerDevTicketsPropuestos(1);
      expect(orm.findUserById).toHaveBeenCalledTimes(1);
      expect(orm.getEquiposByUser).toHaveBeenCalledTimes(1);
      expect(orm.getTicketsPropByTeam).toHaveBeenCalledTimes(0);
      expect(result.statusCode).toEqual(200);
      expect(result.error).toEqual(
        "Desarrollador no pertenece a ningun equipo"
      );
    });
    test("Test 3. Comprobar obtnerDevTicketsPropuestos con desarrolador y con equipo, pero no hay tickets propuestos", async () => {
      orm.findUserById.mockReturnValueOnce({
        nombre: "Esteban",
        apellido: "Gonzales",
        correo: "egonza@gmail.com",
      });
      orm.getEquiposByUser.mockReturnValueOnce([
        {
          id: 1,
          nombre: "Equipo 1",
        },
      ]);
      orm.getTicketsPropByTeam.mockReturnValueOnce([]);
      const result = await controller.obtnerDevTicketsPropuestos(1);
      expect(orm.findUserById).toHaveBeenCalledTimes(1);
      expect(orm.getEquiposByUser).toHaveBeenCalledTimes(1);
      expect(orm.getTicketsPropByTeam).toHaveBeenCalledTimes(1);
      expect(result.statusCode).toEqual(200);
      expect(result.error).toEqual("No hay tickets propuestos");
    });
  });

  describe("Test Create New Ticket Function", () => {
    test("Crear nuevo ticket correctamente", async () => {
      orm.createNewTicket.mockReturnValueOnce({
        titulo: "hola",
        nombre_reportero: "tomas",
        correo_reportero: "tomas@gmail.com",
        app_id: "halo",
        estado_id: "Nuevo",
        descripcion: "esto fue lo que paso",
        pasos: "haciendo esto pasa lo que pasa",
      });

      const result = await controller.storeNewTicket(
        "hola",
        "tomas",
        "tomas@gmail.com",
        "halo",
        "Nuevo",
        "esto fue lo que paso",
        "haciendo esto pasa lo que pasa",
        null
      );
      expect(orm.createNewTicket).toHaveBeenCalledTimes(1);
      expect(result.statusCode).toEqual(201);
    });

    test("Crear nuevo ticket devuelve ticket vacio", async () => {
      orm.createNewTicket.mockReturnValueOnce({});

      const result = await controller.storeNewTicket(
        "hola",
        "tomas",
        "tomas@gmail.com",
        "halo",
        "Nuevo",
        "esto fue lo que paso",
        "haciendo esto pasa lo que pasa",
        null
      );
      expect(orm.createNewTicket).toHaveBeenCalledTimes(1);
      expect(result.statusCode).toEqual(500);
    });

    test("Crear nuevo ticket devuelve null", async () => {
      orm.createNewTicket.mockReturnValueOnce(null);

      const result = await controller.storeNewTicket(
        "hola",
        "tomas",
        "tomas@gmail.com",
        "halo",
        "Nuevo",
        "esto fue lo que paso",
        "haciendo esto pasa lo que pasa",
        null
      );
      expect(orm.createNewTicket).toHaveBeenCalledTimes(1);
      expect(result.statusCode).toEqual(500);
    });
  });
  describe("Test funcionn Crear nuevo comentario", () => {
    test("Create New Comment Function correctamente", async () => {
      orm.createNewComment.mockReturnValueOnce({
        id: 1,
        contenido: "Nuevo Comentario",
        user_Id: 1,
        ticket_id: 1,
      });
      const result = await controller.storeNewComment("Nuevo Comentario", 1, 1);
      expect(orm.createNewComment).toHaveBeenCalledTimes(1);
      expect(result.statusCode).toEqual(201);
    });
    test("Create New Comment Function empty", async () => {
      orm.createNewComment.mockReturnValueOnce({});
      const result = await controller.storeNewComment("Nuevo Comentario", 1, 1);
      expect(orm.createNewComment).toHaveBeenCalledTimes(1);
      expect(result.statusCode).toEqual(500);
    });
    test("Create New Comment Function null", async () => {
      orm.createNewComment.mockReturnValueOnce(null);
      const result = await controller.storeNewComment("Nuevo Comentario", 1, 1);
      expect(orm.createNewComment).toHaveBeenCalledTimes(1);
      expect(result.statusCode).toEqual(500);
    });
  });
  describe("Test Funcion de Reasignar Ticket", () => {
    test("Funcion de Reasignar ticket retorna correctamente", async () => {
      orm.reasignarTicket.mockReturnValueOnce({
        id: 1,
        asignado: null,
      });
      const result = await controller.reasignarTicket(1, 2);
      expect(orm.reasignarTicket).toHaveBeenCalledTimes(1);
      expect(result.statusCode).toEqual(200);
    });
    test("Funcion de Reasignar ticket retorna empty", async () => {
      orm.reasignarTicket.mockReturnValueOnce({});
      const result = await controller.reasignarTicket(1, 2);
      expect(orm.reasignarTicket).toHaveBeenCalledTimes(1);
      expect(result.statusCode).toEqual(500);
    });
    test("Funcion de Reasignar ticket retorna null", async () => {
      orm.reasignarTicket.mockReturnValueOnce(null);
      const result = await controller.reasignarTicket(1, 2);
      expect(orm.reasignarTicket).toHaveBeenCalledTimes(1);
      expect(result.statusCode).toEqual(500);
    });
  });
  describe("Test Funcion de Proponer Reasignar Ticket", () => {
    test("Funcion Proponer Reasignar Ticket logra proponer reasignar el ticket", async () => {
      orm.proponerReasignarTicket.mockReturnValueOnce({
        id: 1,
        asignado: null,
      });
      const result = await controller.proponerReasignarTicket(1, 2);
      expect(orm.proponerReasignarTicket).toHaveBeenCalledTimes(1);
      expect(result.statusCode).toEqual(200)
    });
    test("Funcion Proponer Reasignar Ticket no logra proponer reasignar el ticket", async () => {
      orm.proponerReasignarTicket.mockReturnValueOnce(null);
      const result = await controller.proponerReasignarTicket(1, 2);
      expect(orm.proponerReasignarTicket).toHaveBeenCalledTimes(1);
      expect(result.statusCode).toEqual(500)
    });
  });
});
