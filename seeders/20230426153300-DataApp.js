'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert(
      'software',
      [
        {
          id: 1,
          nombre: 'Memorum',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          id: 2,
          nombre: 'Likof',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          id: 3,
          nombre: 'Digi',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          id: 4,
          nombre: 'Enigma',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          id: 5,
          nombre: 'Orchard',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          id: 6,
          nombre: 'Chimp',
          createdAt: new Date(),
          updatedAt: new Date()
        },
      ])
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("software", null, {});
  }
};
