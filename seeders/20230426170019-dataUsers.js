'use strict';

function generatePassword() {
  const length = 8;
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  let password = '';
  for (let i = 0; i < length; i++) {
    password += characters.charAt(Math.floor(Math.random() * characters.length));
  }
  return password;
}


/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      'usuario',
      [
        {
          nombre: 'Esteban',
          apellido: 'Gonzales',
          correo: 'egonza@gmail.com',
          password: generatePassword(),
          rol_id: 1, // dev
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          nombre: 'Juan',
          apellido: 'Rocha',
          correo: 'jrocha@gmail.com',
          password: generatePassword(),
          rol_id: 1,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          nombre: 'Sebastian',
          apellido: 'Grandon',
          correo: 'sgrandon@gmail.com',
          password: generatePassword(),
          rol_id: 2, //adm
          createdAt: new Date(),
          updatedAt: new Date()
        },
        { nombre: 'Faye', apellido: 'Thurston', correo: 'fayethu@autozone-inc.info', password: generatePassword(), rol_id: 1, createdAt: new Date(), updatedAt: new Date() },
        { nombre: 'Sneha', apellido: 'Baird', correo: 'sneh-bair@careful-organics.org', password: generatePassword(), rol_id: 1, createdAt: new Date(), updatedAt: new Date() },
        { nombre: 'Jermayne', apellido: 'Nieto', correo: 'jermay.nieto@diaperstack.com', password: generatePassword(), rol_id: 1, createdAt: new Date(), updatedAt: new Date() },
        { nombre: 'Narkeasha', apellido: 'Hirsch', correo: 'narkeash_hir@autozone-inc.info', password: generatePassword(), rol_id: 1, createdAt: new Date(), updatedAt: new Date() },
        { nombre: 'Tehya', apellido: 'Mooney', correo: 'te.moone@acusage.net', password: generatePassword(), rol_id: 1, createdAt: new Date(), updatedAt: new Date() },
        { nombre: 'Darrel', apellido: 'Suniga', correo: 'dar.suni@acusage.net', password: generatePassword(), rol_id: 1, createdAt: new Date(), updatedAt: new Date() },
        { nombre: 'Tiffany', apellido: 'Prochaska', correo: 'tipr@diaperstack.com', password: generatePassword(), rol_id: 1, createdAt: new Date(), updatedAt: new Date() },
        { nombre: 'Nyx', apellido: 'Schulze', correo: 'nyschulz@arketmay.com', password: generatePassword(), rol_id: 1, createdAt: new Date(), updatedAt: new Date() },
        { nombre: 'Avenall', apellido: 'Inman', correo: 'ave.inman@consolidated-farm-research.net', password: generatePassword(), rol_id: 1, createdAt: new Date(), updatedAt: new Date() },
        { nombre: 'Finnegan', apellido: 'Bernier', correo: 'finnebe@egl-inc.info', password: generatePassword(), rol_id: 1, createdAt: new Date(), updatedAt: new Date() },
        { nombre: 'Rachael', apellido: 'Hawn', correo: 'racha.hawn@autozone-inc.info', password: generatePassword(), rol_id: 1, createdAt: new Date(), updatedAt: new Date() },
        { nombre: 'Gianni', apellido: 'Snowden', correo: 'gian-snowde@progressenergyinc.info', password: generatePassword(), rol_id: 1, createdAt: new Date(), updatedAt: new Date() },
        { nombre: 'Yobachi', apellido: 'Coppola', correo: 'yobachi.coppola@careful-organics.org', password: generatePassword(), rol_id: 1, createdAt: new Date(), updatedAt: new Date() },
        { nombre: 'Christiana', apellido: 'Carvalho', correo: 'chriscarvalho@egl-inc.info', password: generatePassword(), rol_id: 1, createdAt: new Date(), updatedAt: new Date() },
        { nombre: 'Leah', apellido: 'Nauman', correo: 'lea-nau@arvinmeritor.info', password: generatePassword(), rol_id: 1, createdAt: new Date(), updatedAt: new Date() },
        { nombre: 'Romaine', apellido: 'Schulze', correo: 'romainschu@egl-inc.info', password: generatePassword(), rol_id: 1, createdAt: new Date(), updatedAt: new Date() },
        { nombre: 'Jenny', apellido: 'Snowden', correo: 'jesnowde@consolidated-farm-research.net', password: generatePassword(), rol_id: 1, createdAt: new Date(), updatedAt: new Date() },
        { nombre: 'Jenny', apellido: 'Decosta', correo: 'jedec@diaperstack.com', password: generatePassword(), rol_id: 1, createdAt: new Date(), updatedAt: new Date() },
        { nombre: 'Lychorinda', apellido: 'Surber', correo: 'lychorind.surb@arvinmeritor.info', password: generatePassword(), rol_id: 1, createdAt: new Date(), updatedAt: new Date() },
        { nombre: 'Mick', apellido: 'Dickinson', correo: 'mickdic@autozone-inc.info', password: generatePassword(), rol_id: 1, createdAt: new Date(), updatedAt: new Date() },
        { nombre: 'Jerri', apellido: 'Viveros', correo: 'jer.vivero@consolidated-farm-research.net', password: generatePassword(), rol_id: 1, createdAt: new Date(), updatedAt: new Date() },
        { nombre: 'Yenene', apellido: 'Min', correo: 'yene.mi@autozone-inc.info', password: generatePassword(), rol_id: 1, createdAt: new Date(), updatedAt: new Date() },
        { nombre: 'Gina', apellido: 'Corso', correo: 'gi-cors@progressenergyinc.info', password: generatePassword(), rol_id: 1, createdAt: new Date(), updatedAt: new Date() },
        { nombre: 'Faye', apellido: 'Tudor', correo: 'fa_tudo@acusage.net', password: generatePassword(), rol_id: 1, createdAt: new Date(), updatedAt: new Date() },
        { nombre: 'Shiela', apellido: 'Clinger', correo: 'sh.clinger@arvinmeritor.info', password: generatePassword(), rol_id: 1, createdAt: new Date(), updatedAt: new Date() },
        { nombre: 'Pelton', apellido: 'Axelrod', correo: 'pelt-ax@autozone-inc.info', password: generatePassword(), rol_id: 1, createdAt: new Date(), updatedAt: new Date() },
        { nombre: 'Jerri', apellido: 'Caceres', correo: 'jerr_caceres@acusage.net', password: generatePassword(), rol_id: 1, createdAt: new Date(), updatedAt: new Date() },
        { nombre: 'Mahogany', apellido: 'Lipman', correo: 'mahogany.lip@arvinmeritor.info', password: generatePassword(), rol_id: 1, createdAt: new Date(), updatedAt: new Date() },
        { nombre: 'Zubin', apellido: 'Surber', correo: 'zubsu@careful-organics.org', password: generatePassword(), rol_id: 1, createdAt: new Date(), updatedAt: new Date() },
        { nombre: 'Piyush', apellido: 'Lebel', correo: 'piyu.lebe@egl-inc.info', password: generatePassword(), rol_id: 1, createdAt: new Date(), updatedAt: new Date() },
        { nombre: 'Beagan', apellido: 'Pullum', correo: 'beag-pu@diaperstack.com', password: generatePassword(), rol_id: 1, createdAt: new Date(), updatedAt: new Date() },
        { nombre: 'Saturnino', apellido: 'Manes', correo: 'saturnimane@acusage.net', password: generatePassword(), rol_id: 1, createdAt: new Date(), updatedAt: new Date() },
        { nombre: 'Aran', apellido: 'Markert', correo: 'ara-mar@arketmay.com', password: generatePassword(), rol_id: 1, createdAt: new Date(), updatedAt: new Date() },
        { nombre: 'Estes', apellido: 'Gilliam', correo: 'este-gill@arvinmeritor.info', password: generatePassword(), rol_id: 1, createdAt: new Date(), updatedAt: new Date() },

      ])
  },
  async down(queryInterface, Sequelize) {

    await queryInterface.bulkDelete('usuario', null, {});
  }
};
