'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert(
      'descripcion', 
      [
        { 
          ticket_id: 1,
          detalles: 'Se produjo un error al iniciar la aplicacion',
          pasos: '1. Ingresar correo y contraseña, 2. Apretar boton de iniciar sesión',
          createdAt: new Date(),
          updatedAt : new Date()
        },
        {
          ticket_id: 2,
          detalles: 'Se produjo un error al abrir la lista de participantes del evento, donde los nombres no salian claros',
          pasos: '1. Ingresar al lobbyy, 2. Apretar boton Ver la lista de participantes',
          createdAt: new Date(),
          updatedAt : new Date()
        },
        {
          ticket_id: 3,
          detalles: 'El logo de la aplciacion esta cortado a la mitad lo cual provoca que la aplicacion se vea fea',
          pasos: '1. Ingresar al Lobby y ver logo en la esquina superior',
          createdAt: new Date(),
          updatedAt : new Date()
        },
        {
          ticket_id: 4,
          detalles: 'Se produjo un error al buscar los elementos, donde el software entrega elementos nada que ver con lo que pedido',
          pasos: '1. Ingresar al Lobby. 2. Ingresar a la pestaña de búsqueda, 3. Ingresar busqueda de algun objeto por ejemplo pizza ',
          createdAt: new Date(),
          updatedAt : new Date()
        },
        {
          ticket_id: 5,
          detalles: 'Se produjo un error al buscar los elementos, donde el software entrega elementos nada que ver con lo que pedido',
          pasos: '1. Ingresar al Lobby. 2. Ingresar a la pestaña de búsqueda, 3. Ingresar busqueda de algun objeto por ejemplo pizza ',
          createdAt: new Date(),
          updatedAt : new Date()
        },
        {
          ticket_id: 6,
          detalles: 'Se produjo un error al buscar los elementos, donde el software entrega elementos nada que ver con lo que pedido',
          pasos: '1. Ingresar al Lobby. 2. Ingresar a la pestaña de búsqueda, 3. Ingresar busqueda de algun objeto por ejemplo pizza ',
          createdAt: new Date(),
          updatedAt : new Date()
        },
      ]
    )
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('descripcion', null, {});
  }
};
