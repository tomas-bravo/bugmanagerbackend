'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert(
      'equipoticket_propuesto',
      [
        {
          equipo_id: 1,
          ticket_id: 5,
          createdAt: new Date(),
          updatedAt : new Date()
        },
        {
          equipo_id: 2,
          ticket_id: 6,
          createdAt: new Date(),
          updatedAt : new Date()
        }
      ]
    )    
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('equipoticket_propuesto', null, {});
  }
};
