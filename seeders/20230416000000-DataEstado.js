'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {

    await queryInterface.bulkInsert('estado', [
      {
        nombre: 'Nuevo',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        nombre: 'Asignado',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        nombre: 'En Progreso',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        nombre: 'Finalizado',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        nombre: 'Reasignado',
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ],);
  },

  async down(queryInterface, Sequelize) {

    await queryInterface.bulkDelete('estado', null, {});
  }
};
