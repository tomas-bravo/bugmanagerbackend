'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class software extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      //Opcion 1
      /*software.belongsToMany(models.equipo, {
        through: "equipoapp_trabajoen",
        as : "equipo",
        foreignKey: "app_id"
      })*/
      //Opcion 2
      software.belongsToMany(models.equipo, {
        through: models.equipoapp_trabajoen,
        foreignKey: 'app_id'
      })
      software.hasMany(models.ticket, { foreignKey: 'app_id' })
      // define association here

    }
  }
  software.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    nombre: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'software',
  });
  return software;
};