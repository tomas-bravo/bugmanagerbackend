'use strict';
const {
  Model
} = require('sequelize');
const ticket = require('./ticket');
const { createItemFromDescriptor } = require('@babel/core/lib/config/item');
module.exports = (sequelize, DataTypes) => {
  class equipoticket_propuesto extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      //Opcion 2
      // equipoticket_propuesto.belongsToMany(models.equipo, {foreignKey: 'equipo_id'})
      // equipoticket_propuesto.belongsTo(models.ticket, {foreignKey: 'ticket_id'})
      // equipoticket_propuesto.hasOne(models.equipo, { foreignKey: 'equipo_id'})
      // equipoticket_propuesto.hasOne(models.ticket,{ foreignKey: 'ticket_id'})
      // define association here
    }
  }
  equipoticket_propuesto.init({
    equipo_id: DataTypes.INTEGER,
    ticket_id: DataTypes.INTEGER,
    createdAt: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'equipoticket_propuesto',
  });
  return equipoticket_propuesto;
};