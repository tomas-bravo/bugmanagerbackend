"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    class ticket extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            ticket.belongsToMany(models.usuario, {
                through: "ticketusuario_aceptado",
                as: "aceptado",
                foreignKey: "ticket_id",
            });
            ticket.belongsToMany(models.usuario, {
                through: "comentario",
                as: "comment",
                foreignKey: "ticket_id",
            });
            ticket.belongsTo(models.usuario, { as: 'asignado', foreignKey: 'usuarioId' });
            /*ticket.belongsToMany(models.usuario, {
              through: "informe",
              as: "report",
              foreignKey: "ticket_id"
            })*/
            ticket.hasMany(models.informe, { foreignKey: "ticket_id" });
            ticket.hasOne(models.descripcion, { foreignKey: "ticket_id" });
            ticket.hasMany(models.imagen, { foreignKey: "ticket_id" });
            ticket.belongsTo(models.software, { foreignKey: "app_id" });
            ticket.belongsToMany(models.equipo, {
                through: "equipoticket_propuesto",
                foreignKey: "ticket_id",
            });
            ticket.belongsTo(models.nivelprioridad, {
                foreignKey: "nivelPrioridad_id",
            });
            ticket.belongsTo(models.estado, { foreignKey: "estado_id" });
            ticket.belongsToMany(models.usuario, { through: 'reasignar', as: 'reassignment' })
        }
    }
    ticket.init(
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            titulo: DataTypes.STRING,
            nombre_reportero: DataTypes.STRING,
            correo_reportero: DataTypes.STRING,
            estado_id: DataTypes.INTEGER,
            nivelPrioridad_id: DataTypes.INTEGER,
            app_id: DataTypes.INTEGER,
            usuarioId: DataTypes.INTEGER,
            createdAt: DataTypes.DATE,
        },
        {
            sequelize,
            modelName: "ticket",
        }
    );
    return ticket;
};
