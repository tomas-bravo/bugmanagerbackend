'use strict';
const {
  Model
} = require('sequelize');
const ticket = require('./ticket');
module.exports = (sequelize, DataTypes) => {
  class comentario extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      comentario.belongsTo(models.ticket, { foreignKey: 'ticket_id'})
      comentario.belongsTo(models.usuario, { foreignKey: 'usuario_id'})
      // define association here
    }
  }
  comentario.init({
    ticket_id:{
      type: DataTypes.INTEGER,
      primaryKey : true,
      allowNull: false
    }, 
    usuario_id: {
      type: DataTypes.INTEGER,
      primaryKey : true,
      allowNull: false
    },
    contenido: DataTypes.TEXT,
    createdAt: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'comentario',
  });
  return comentario;
};