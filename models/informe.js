
'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class informe extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      informe.belongsTo(models.tipo, { foreignKey: 'tipo_id'})
      informe.belongsTo(models.usuario, { foreignKey: 'usuario_id'})
      informe.belongsTo(models.ticket, { foreignKey: 'ticket_id'})
      // define association here
    }
  }
  informe.init({
    id:{
      type: DataTypes.INTEGER,
      primaryKey : true,
      autoIncrement: true
    },
    usuario_id: {
      type: DataTypes.INTEGER,
    },
    ticket_id :{
      type: DataTypes.INTEGER
    },
    tipo_id: {
      type: DataTypes.INTEGER,
    },
    titulo: DataTypes.STRING,
    contenido: DataTypes.TEXT,
    createdAt: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'informe',
  });
  return informe;
};