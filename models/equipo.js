'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class equipo extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      // Opcion 1
      /*equipo.belongsToMany(models.usuario, {
        through: 'usuarioequipo_esparte',
        as : 'usuario',
        foreignKey: 'equipo_id'
      }),
      equipo.belongsToMany(models.software, {
        through: 'equipoapp_trabajoen',
        as : 'software',
        foreignKey: 'equipo_id'
      }),
      equipo.belongsToMany(models.ticket, {
        through: "equipoticket_propuesto",
        as : "ticket",
        foreignKey: "equipo_id"
      })*/
      //Opcion 2
      equipo.belongsToMany(models.software, {
        through: models.equipoapp_trabajoen,
        foreignKey: "equipo_id",
      })
      equipo.belongsToMany(models.usuario, {
        through: 'usuarioequipo_esparte',
        // as: 'usuarios',
        foreignKey: 'equipo_id'
      })
      equipo.belongsToMany(models.ticket, {
        through: 'equipoticket_propuesto',
        foreignKey: 'equipo_id',
      })
    }
  }
  equipo.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    nombre: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'equipo',
  });
  return equipo;
};