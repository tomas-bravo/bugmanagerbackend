'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('informe', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      usuario_id: {
        //primaryKey: true,
        allowNull: false,
        type: Sequelize.INTEGER,
        onDelete : 'CASCADE',
        onUpdate : 'CASCADE',
        references:{
          model: 'usuario',
          key:  'id'
        }
      },
      ticket_id: {
        //primaryKey: true,
        allowNull: false,
        type: Sequelize.INTEGER,
        onDelete : 'CASCADE',
        onUpdate : 'CASCADE',
        references:{
          model: 'ticket',
          key:  'id'
        }
      },
      tipo_id:{
        onDelete : 'CASCADE',
        //primaryKey: true,
        onUpdate : 'CASCADE',
        allowNull: false,
        type: Sequelize.INTEGER,
        references:{
          model: 'tipo',
          key:  'id'
        }
      },
      titulo: {
        allowNull: false,
        type: Sequelize.STRING
      },
      contenido: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('informe');
  }
};
